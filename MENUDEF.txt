OptionMenu "Baron Options"
{
	Title "Baron Options"

	StaticText ""
	StaticText "----- Gameplay Options -----"
	StaticText ""

	Option "Bruiser Sphere Spawns", "bruiser_spawn", "OnOff"
	Option "Use Old Jump Behaviour", "old_jump", "OnOff"

	StaticText ""
	StaticText "----- Visual Options -----"
	StaticText ""
	Option "Auxiliary Flames", "hod_flames", "OnOff"
	
	Option "Smoke", "hod_smoke", "OnOff"
	
	Option "Embers and Sparks", "hod_embers", "OnOff"
	
	Option "Rubble", "hod_rubble", "OnOff"

	StaticText ""
	StaticText "----- Sound Options -----"
	StaticText ""
	
	Slider "Footstep Volume", "fs_sound", 0 , 1.0, 0.02, 2
}

AddOptionMenu "OptionsMenu"
{
    StaticText ""
    Submenu "Heart of Demons - BARON", "Baron Options"
}