class BaronHands : BaronBaseWeapon replaces Pistol
{
	enum Hands
	{
		Left = 3,
		Right = 4
	}
	
	override void PostBeginPlay()
	{
		Boosted = false;
		
		ChargeLeft = 0;
		ChargeRight = 0;
		
		Super.PostBeginPlay();
	}
	
	//-------------------------------------------------------------------------------------------
	//
	// FUNCTIONS THAT YOU ***DON'T*** CHANGE
	//
	//-------------------------------------------------------------------------------------------

	private action int A_GetNextSpellIndex(int currentspell)
	{
		let plr = TheBaron(self);
		if (plr)
		{
			//Wraps around the spells. Gets the last available spell first.
			int index = 0;
			for (int i = index; i < plr.AvailableSpells.Size(); ++i)
				if (plr.AvailableSpells[i]) index = i;
			
			if (currentspell == index) return 0;
				
			for (int i = currentspell + 1; i < plr.AvailableSpells.Size(); ++i) //Skips unavailable spells.
			{
				if (!plr.AvailableSpells[i]) continue;
				else return i;
			}
		}
			
		return -1; //This should never happen.
	}
	
	private action int A_GetPreviousSpellIndex(int currentspell)
	{
		let plr = TheBaron(self);
		if (plr)
		{
			//Wraps around the spells. Gets the last available spell first.
			int index = 0;
			for (int i = index; i < plr.AvailableSpells.Size(); ++i)
				if (plr.AvailableSpells[i]) index = i;
			
			if (currentspell == 0) return index; //Base spell. Wraps around the spells.
				
			for (int i = currentspell - 1; i >= 0; --i) //Skips unavailable spells.
			{
				if (!plr.AvailableSpells[i]) continue;
				else return i;
			}
		}
			
		return -1; //This should never happen.
	}
	
	private action state A_CheckButtons(int hand)
	{
		int buttons = player.cmd.buttons;
		int oldbuttons = player.oldbuttons;
		
		let plr = TheBaron(self);
		
		switch(hand)
		{
			case Left:
				if (CountInv("WeaponIsReady") == 1)
				{
					if (player.cmd.buttons & BT_ZOOM && plr && !plr.ShowSelectMenuLeft)
					{
						plr.ShowSelectMenuLeft = true;
						return ResolveState(null);
					}
					
					if (player.cmd.buttons & BT_USER1) return ResolveState("LeftBoost");
					
					if (plr.ShowSelectMenuLeft)
					{
						if (buttons & BT_ATTACK && !(oldbuttons & BT_ATTACK))
							plr.SelectedSpellLeft = A_GetPreviousSpellIndex(plr.SelectedSpellLeft);
						
						if (buttons & BT_ALTATTACK && !(oldbuttons & BT_ALTATTACK))
							plr.SelectedSpellLeft = A_GetNextSpellIndex(plr.SelectedSpellLeft);
						
						if (buttons & BT_ZOOM && !(oldbuttons & BT_ZOOM))
						{
							plr.ShowSelectMenuLeft = false;
							return ResolveState("LeftSwitch");
						}
					}
					else if (buttons & BT_ATTACK && !plr.ShowSelectMenuRight) return A_SpellAttack(Left);
				}
				break;
				
			case Right:
				if (CountInv("WeaponIsReady") == 1)
				{
					if (player.cmd.buttons & BT_RELOAD && plr && !plr.ShowSelectMenuRight)
					{
						plr.ShowSelectMenuRight = true;
						return ResolveState(null);
					}
					
					if (player.cmd.buttons & BT_USER1) return ResolveState("RightBoost");
					
					if (plr.ShowSelectMenuRight)
					{
						if (buttons & BT_ATTACK && !(oldbuttons & BT_ATTACK))
							plr.SelectedSpellRight = A_GetPreviousSpellIndex(plr.SelectedSpellRight);
						
						if (buttons & BT_ALTATTACK && !(oldbuttons & BT_ALTATTACK))
							plr.SelectedSpellRight = A_GetNextSpellIndex(plr.SelectedSpellRight);
						
						if (buttons & BT_RELOAD && !(oldbuttons & BT_RELOAD))
						{
							plr.ShowSelectMenuRight = false;
							return ResolveState("RightSwitch");
						}
					}
					else if (buttons & BT_ALTATTACK && !plr.ShowSelectMenuLeft) return A_SpellAttack(Right);
				}
				break;
		}
		return ResolveState(null);
	}
	
	private action state A_ReFireHands(int hand, StateLabel jumpstate)
	{
		switch(hand)
		{
			case Left: if (player.cmd.buttons & BT_ATTACK) return ResolveState(jumpstate); break;
			case Right: if (player.cmd.buttons & BT_ALTATTACK) return ResolveState(jumpstate); break;
		}
		return ResolveState(null);
	}
	
	private action void A_ChargeAttack(int hand, int maxTics = 100)
	{
		switch(hand)
		{
			case Left: invoker.ChargeLeft = clamp(++invoker.ChargeLeft, 0, maxTics); break;
			case Right: invoker.ChargeRight = clamp(++invoker.ChargeRight, 0, maxTics); break;
		}
	}
	
	//divisionTics defines how many charge tics the player must have accumulated to gain +1 charge level.
	//In layman's terms, the lower the number, the more charges you can have and the faster you'll gain charges.
	action int A_GetChargeLevel(int hand, int divisionTics = 25)
	{
		switch(hand)
		{
			//These breaks should never be reached due to the return, but I don't trust ZScript's switch after that one time. -Ace
			case Left: return invoker.ChargeLeft / divisionTics; break;
			case Right: return invoker.ChargeRight / divisionTics; break;
		}
		return 0;
	}
	
	//-------------------------------------------------------------------------------------------
	//
	// FUNCTIONS THAT YOU CHANGE
	//
	//-------------------------------------------------------------------------------------------
	
	private action void A_SetWeaponSprite(int hand)
	{
		if (invoker.owner && invoker.owner.player && invoker.owner.player.health > 0)
		{
			let psp = invoker.owner.player.FindPSprite(hand);
			let plr = TheBaron(self);
			if (psp && plr)
			{
				switch(hand)
				{
					case Left:
						switch(plr.SelectedSpellLeft)
						{
							case plr.Normal: psp.sprite = invoker.Boosted ? GetSpriteIndex("TBLG") : GetSpriteIndex("BBLG"); break;
							case plr.Fireball: psp.sprite = invoker.Boosted ? GetSpriteIndex("TFLG") : GetSpriteIndex("FBLG"); break;
							case plr.Immolation: psp.sprite = invoker.Boosted ? GetSpriteIndex("BILG") : GetSpriteIndex("IMLG"); break;
							case plr.Meteor: psp.sprite = invoker.Boosted ? GetSpriteIndex("BMLG") : GetSpriteIndex("MTLG"); break;
							case plr.Defile: psp.sprite = invoker.Boosted ? GetSpriteIndex("BCLG") : GetSpriteIndex("CPLG"); break;
							case plr.Hydra: psp.sprite = invoker.Boosted ? GetSpriteIndex("BHLG") : GetSpriteIndex("HYLG"); break;
							case plr.Clone: psp.sprite = invoker.Boosted ? GetSpriteIndex("TCLG") : GetSpriteIndex("CLLG"); break;
						}
						break;
						
					case Right:
						switch(plr.SelectedSpellRight)
						{
							case plr.Normal: psp.sprite = invoker.Boosted ? GetSpriteIndex("TBRG") : GetSpriteIndex("BBRG"); break;
							case plr.Fireball: psp.sprite = invoker.Boosted ? GetSpriteIndex("TFRG") : GetSpriteIndex("FBRG"); break;
							case plr.Immolation: psp.sprite = invoker.Boosted ? GetSpriteIndex("BIRG") : GetSpriteIndex("IMRG"); break;
							case plr.Meteor: psp.sprite = invoker.Boosted ? GetSpriteIndex("BMRG") : GetSpriteIndex("MTRG"); break;
							case plr.Defile: psp.sprite = invoker.Boosted ? GetSpriteIndex("BCRG") : GetSpriteIndex("CPRG"); break;
							case plr.Hydra: psp.sprite = invoker.Boosted ? GetSpriteIndex("BHRG") : GetSpriteIndex("HYRG"); break;
							case plr.Clone: psp.sprite = invoker.Boosted ? GetSpriteIndex("TCRG") : GetSpriteIndex("CLRG"); break;
						}
						break;
				}
			}
		}
	}
	
	private action state A_ResolveReady(int hand)
	{
		A_OverlayFlags(hand, PSPF_ADDBOB, true);
		
		let plr = TheBaron(self);
		if (plr)
		{
			switch(hand) //Add your breaks, kids. Weird shit can happen if you don't. -Ace
			{
				case Left:
					switch(plr.SelectedSpellLeft)
					{
						case plr.Normal: return invoker.Boosted ? ResolveState("BoostedNormalReadyLeft") : ResolveState("NormalReadyLeft");
						case plr.Fireball: return invoker.Boosted ? ResolveState("BoostedFireballReadyLeft") : ResolveState("FireballReadyLeft");
						case plr.Immolation: return invoker.Boosted ? ResolveState("BoostedImmolationReadyLeft") : ResolveState("ImmolationReadyLeft");
						case plr.Meteor: return invoker.Boosted ? ResolveState("BoostedMeteorReadyLeft") : ResolveState("MeteorReadyLeft");
						case plr.Defile: return invoker.Boosted ? ResolveState("BoostedDefileReadyLeft") : ResolveState("DefileReadyLeft");
						case plr.Hydra: return invoker.Boosted ? ResolveState("BoostedHydraReadyLeft") : ResolveState("HydraReadyLeft");
						case plr.Clone: return invoker.Boosted ? ResolveState("BoostedCloneReadyLeft") : ResolveState("CloneReadyLeft");
					}
					break;
				case Right:
					switch(plr.SelectedSpellRight)
					{
						case plr.Normal: return invoker.Boosted ? ResolveState("BoostedNormalReadyRight") : ResolveState("NormalReadyRight");
						case plr.Fireball: return invoker.Boosted ? ResolveState("BoostedFireballReadyRight") : ResolveState("FireballReadyRight");
						case plr.Immolation: return invoker.Boosted ? ResolveState("BoostedImmolationReadyRight") : ResolveState("ImmolationReadyRight");
						case plr.Meteor: return invoker.Boosted ? ResolveState("BoostedMeteorReadyRight") : ResolveState("MeteorReadyRight");
						case plr.Defile: return invoker.Boosted ? ResolveState("BoostedDefileReadyRight") : ResolveState("DefileReadyRight");
						case plr.Hydra: return invoker.Boosted ? ResolveState("BoostedHydraReadyRight") : ResolveState("HydraReadyRight");
						case plr.Clone: return invoker.Boosted ? ResolveState("BoostedCloneReadyRight") : ResolveState("CloneReadyRight");
					}
					break;
			}
		}
		return ResolveState(null);
	}
	
	private action state A_SpellAttack(int hand)
	{
		let plr = TheBaron(self);
		if (plr)
		{
			switch(hand)
			{
				case Left:
					switch(plr.SelectedSpellLeft)
					{
						case plr.Normal: return invoker.Boosted ? ResolveState("BoostedNormalFireLeft") : ResolveState("NormalFireLeft");
						case plr.Fireball: return invoker.Boosted ? ResolveState("BoostedFireballFireLeft") : ResolveState("FireballFireLeft");
						case plr.Immolation: return invoker.Boosted || CheckInventory("PowerWeaponLevel2", 0) ? ResolveState("BoostedImmolationFireLeft") : ResolveState("ImmolationFireLeft");
						case plr.Meteor: return invoker.Boosted ? ResolveState("BoostedMeteorFireLeft") : ResolveState("MeteorFireLeft");
						case plr.Defile: return invoker.Boosted ? ResolveState("BoostedDefileFireLeft") : ResolveState("DefileFireLeft");
						case plr.Hydra: return invoker.Boosted ? ResolveState("BoostedHydraFireLeft") : ResolveState("HydraFireLeft");
						case plr.Clone: return invoker.Boosted ? ResolveState("BoostedCloneFireLeft") : ResolveState("CloneFireLeft");
					}
					break;
				case Right:
					switch(plr.SelectedSpellRight)
					{
						case plr.Normal: return invoker.Boosted ? ResolveState("BoostedNormalFireRight") : ResolveState("NormalFireRight");
						case plr.Fireball: return invoker.Boosted ? ResolveState("BoostedFireballFireRight") : ResolveState("FireballFireRight");
						case plr.Immolation: return invoker.Boosted || CheckInventory("PowerWeaponLevel2", 0) ? ResolveState("BoostedImmolationFireRight") : ResolveState("ImmolationFireRight");
						case plr.Meteor: return invoker.Boosted ? ResolveState("BoostedMeteorFireRight") : ResolveState("MeteorFireRight");
						case plr.Defile: return invoker.Boosted ? ResolveState("BoostedDefileFireRight") : ResolveState("DefileFireRight");
						case plr.Hydra: return invoker.Boosted ? ResolveState("BoostedHydraFireRight") : ResolveState("HydraFireRight");
						case plr.Clone: return invoker.Boosted ? ResolveState("BoostedCloneFireRight") : ResolveState("CloneFireRight");
					}
					break;
			}
		}
		return ResolveState(null);
	}

	bool Boosted;
	
	//Primarily used for Fireball, but could be used for pretty much anything.
	int ChargeLeft;
	int ChargeRight;
	
	Default
	{
		Inventory.PickUpMessage "Seriously, doe, how did you drop this? WHY would you drop this?";
		Tag "Spell Hands";
		Weapon.SlotNumber 2;
		Weapon.BobStyle "Smooth";
		Weapon.BobRangeX 0.25;
		Weapon.BobRangeY 0.1;
		Weapon.BobSpeed 0.5;
		+WEAPON.NOALERT
	}
	
	States
	{
		//------------------------------------------------
		//
		// SPELL SWITCH
		//
		//------------------------------------------------
		
		LeftSwitch:
			#### AAAAA 1 Bright A_OverlayOffset(Left, 0, 16, WOF_ADD);
			#### A 1 Bright A_SetWeaponSprite(Left);
			TNT1 A 0
			{
				A_PlaySound("effects/SpellSwitch", CHAN_6);
			}
			#### AAAAA 1 Bright A_OverlayOffset(Left, 0, -16, WOF_ADD);
			Goto LeftHand;
			
		RightSwitch:
			#### AAAAA 1 Bright A_OverlayOffset(Right, 0, 16, WOF_ADD);
			#### A 1 Bright A_SetWeaponSprite(Right);
			TNT1 A 0
			{
				A_PlaySound("effects/SpellSwitch", CHAN_6);
			}
			#### AAAAA 1 Bright A_OverlayOffset(Right, 0, -16, WOF_ADD);
			Goto RightHand;
			
		//-----------------------------------------------
		//
		// BOOST
		//
		//------------------------------------------------	
		
		//This hack is needed so that boost is toggled properly, otherwise it will glitch out and not set the sprite/bool correctly. -Ace
		User1:
			TNT1 A 15
			{
				let plr = TheBaron(self);
				if (!plr.SpellBoostRune)
				{
					A_Print("Spell Booster Missing", 2);
					return ResolveState("Ready");
				}
				else invoker.Boosted = !invoker.Boosted;
					
				return ResolveState(null);
			}
			Goto Ready;
		
		LeftBoost:
			#### A 0
			{
				let plr = TheBaron(self);
				if (!plr.SpellBoostRune)
					return ResolveState("LeftHand");
				return ResolveState(null);
			}
			#### AAAAA 1 Bright A_OverlayOffset(Left, 0, 16, WOF_ADD);
			#### A 1 Bright A_SetWeaponSprite(Left);
			#### AAAAA 1 Bright A_OverlayOffset(Left, 0, -16, WOF_ADD);
			Goto LeftHand;
			
		RightBoost:
			#### A 0
			{
				let plr = TheBaron(self);
				if (!plr.SpellBoostRune)
					return ResolveState("RightHand");
				return ResolveState(null);
			}
			#### AAAAA 1 Bright A_OverlayOffset(Right, 0, 16, WOF_ADD);
			#### A 1 Bright A_SetWeaponSprite(Right);
			#### AAAAA 1 Bright A_OverlayOffset(Right, 0, -16, WOF_ADD);
			Goto RightHand;
	
		//------------------------------------------------
		//
		// SELECT & DESELECT
		//
		//------------------------------------------------
		
		Select:
			TNT1 A 0
			{
				A_Overlay(Left, "LeftHand");
				A_Overlay(Right, "RightHand");
			}
		SelectMain:
			TNT1 A 1 Bright A_Raise(6 * 2);
			Loop;
		Deselect:
			TNT1 A 1 Bright
			{
				let plr = TheBaron(self);
				if (plr)
				{
					plr.ShowSelectMenuLeft = false;
					plr.ShowSelectMenuRight = false;
				}
				A_TakeInventory("WeaponIsReady");
				A_Lower(6 * 2);
			}
			Loop;
		DeadLowered:
			TNT1 A -1 Bright;
			Stop;
	
		//------------------------------------------------
		//
		// READY STATES
		//
		//------------------------------------------------
		
		Ready:
			TNT1 A 1 Bright
			{
				A_GiveInventory("WeaponIsReady");
				A_WeaponReady(WRF_ALLOWUSER1);
			}
			Loop;
		
		// --------------- OVERLAYS ---------------
		
		LeftHand:
			#### A 1 Bright A_SetWeaponSprite(Left); //I don't remember what these do, but won't touch them or else something might break. -Ace
			#### A 0 A_ResolveReady(Left);
			Stop;
			
		RightHand:
			#### A 1 Bright A_SetWeaponSprite(Right);
			#### A 0 A_ResolveReady(Right);
			Stop;
			
			
		// --------------- NORMAL ---------------
		
		NormalReadyLeft:
			BBLG AABBCCDDEE 1 Bright A_CheckButtons(Left); //More frames, smaller duration: keeps the animation, but checks for input more often.
			Loop;	
		NormalReadyRight:
			BBRG AABBCCDDEE 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedNormalReadyLeft:
			TBLG AABBCCDDEEDDCCBB 1 Bright A_CheckButtons(Left); //More frames, smaller duration: keeps the animation, but checks for input more often.
			Loop;
		BoostedNormalReadyRight:
			TBRG AABBCCDDEEDDCCBB 1 Bright A_CheckButtons(Right);
			Loop;
		
		// --------------- FIREBALL ---------------
		
		FireballReadyLeft:
			FBLG AABBCCDDEE 1 Bright A_CheckButtons(Left);
			Loop;
		FireballReadyRight:
			FBRG AABBCCDDEE 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedFireballReadyLeft:
			TFLG AABBCCDDEE 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedFireballReadyRight:
			TFRG AABBCCDDEE 1 Bright A_CheckButtons(Right);
			Loop;
			
		// --------------- IMMOLATION ---------------
		
		ImmolationReadyLeft:
			IMLG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Left);
			Loop;
		ImmolationReadyRight:
			IMRG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedImmolationReadyLeft:
			BILG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedImmolationReadyRight:
			BIRG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Right);
			Loop;
			
		// --------------- METEOR ---------------
		
		MeteorReadyLeft:
			MTLG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Left);
			Loop;
		MeteorReadyRight:
			MTRG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedMeteorReadyLeft:
			BMLG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedMeteorReadyRight:
			BMRG AABBCCDDEEFFGG 1 Bright A_CheckButtons(Right);
			Loop;
			
		// --------------- DEFILE ---------------
		
		DefileReadyLeft:
			CPLG AABBCCDD 1 Bright A_CheckButtons(Left);
			Loop;
		DefileReadyRight:
			CPRG AABBCCDD 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedDefileReadyLeft:
			BCLG AABBCCDD 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedDefileReadyRight:
			BCRG AABBCCDD 1 Bright A_CheckButtons(Right);
			Loop;
			
		// --------------- HYDRA ---------------
		
		HydraReadyLeft:
			HYLG AABBCC 1 Bright A_CheckButtons(Left);
			Loop;
		HydraReadyRight:
			HYRG AABBCC 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedHydraReadyLeft:
			BHLG AABBCC 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedHydraReadyRight:
			BHRG AABBCC 1 Bright A_CheckButtons(Right);
			Loop;
			
		// --------------- CLONE ---------------
		
		CloneReadyLeft:
			CLLG AABBCC 1 Bright A_CheckButtons(Left);
			Loop;
		CloneReadyRight:
			CLRG AABBCC 1 Bright A_CheckButtons(Right);
			Loop;
			
		BoostedCloneReadyLeft:
			TCLG AABBCC 1 Bright A_CheckButtons(Left);
			Loop;
		BoostedCloneReadyRight:
			TCRG AABBCC 1 Bright A_CheckButtons(Right);
			Loop;
			
		//------------------------------------------------
		//
		// FIRE STATES
		//
		//------------------------------------------------
		
		Fire: //Disabled.
			Goto Ready;
		
		// --------------- NORMAL ---------------
		
		NormalFireLeft:
			BBLF A 0 A_TakeMana(1, false, "LeftHand", 20);
			BBLF A 0
			{
				A_PlaySound("weapons/SpellCast", chan_6);
				A_AlertMonsters();
			}
			BBLG ABC 1 Bright A_OverlayOffset(Left, 24, 2.5, WOF_ADD); 
			BBLF AA 1 Bright A_OverlayOffset(Left, 1, 0.25, WOF_ADD);
			BBLF A 1 Bright;
			BBLF A 1 Bright A_OverlayOffset(Left, -16, -2, WOF_ADD);
			BBLF B 1 Bright A_OverlayOffset(Left, -24, -2, WOF_ADD); 
			BBLF C 1 Bright
			{
				if(CheckInventory("PowerWeaponLevel2", 0))
					A_FireProjectile("BoostBaronBall", 0, false, -6, 0, FPF_NOAUTOAIM);
				else A_FireProjectile("NewBaronBall", 0, false, -6, 0, FPF_NOAUTOAIM);
				A_OverlayOffset(Left, -36, -1, WOF_ADD); 
			}
			BBLF C 1 A_OverlayOffset(Left, -64, 0, WOF_ADD); 
			//BBLF C 1 A_OverlayOffset(Left, -64, 32, WOF_ADD); 
			BBLF C 1 A_OverlayOffset(Left, -64, 4, WOF_ADD); 
			TNT1 A 3 A_OverlayOffset(Left, 88, 88, WOF_INTERPOLATE);
			BBLG DCBA 1 Bright A_OverlayOffset(Left, -22, -22, WOF_ADD);
			Goto LeftHand;
			
		NormalFireRight:
			BBRF A 0 A_TakeMana(1, false, "RightHand", 20);
			BBRF A 0
			{
				A_PlaySound("weapons/SpellCast", chan_7);
				A_AlertMonsters();
			}
			BBRG ABC 1 Bright A_OverlayOffset(Right, -24, 2.5, WOF_ADD); 
			BBRF AA 1 Bright A_OverlayOffset(Right, -1, 0.25, WOF_ADD);
			BBRF A 1 Bright;
			BBRF A 1 Bright A_OverlayOffset(Right, 16, -2, WOF_ADD);
			BBRF B 1 Bright A_OverlayOffset(Right, 24, -2, WOF_ADD); 
			BBRF C 1 Bright
			{
				if(CheckInventory("PowerWeaponLevel2", 0))
					A_FireProjectile("BoostBaronBall", 0, false, 6, 0, FPF_NOAUTOAIM);
				else A_FireProjectile("NewBaronBall", 0, false, 6, 0, FPF_NOAUTOAIM);
				A_OverlayOffset(Right, 36, -1, WOF_ADD);
			}
			BBRF C 1 A_OverlayOffset(Right, 64, 0, WOF_ADD);
			//BBRF CC 1 A_OverlayOffset(Right, 32, 32, WOF_ADD);
			BBRF C 1 A_OverlayOffset(Right, 64, 4, WOF_ADD);
			TNT1 A 3 A_OverlayOffset(Right, -88, 88, WOF_INTERPOLATE);
			BBRG DCBA 1 Bright A_OverlayOffset(Right, 22, -22, WOF_ADD);
			Goto RightHand;
			
		// --------------- BOOSTED NORMAL ---------------
		
		BoostedNormalFireLeft:
			TBLF A 0 A_TakeMana(2, false, "LeftHand", 10);
			TBLF A 0
			{
				A_PlaySound("weapons/SpellCast", chan_7);
				A_AlertMonsters();
			}
			TBLG A 1 Bright A_OverlayOffset(Left, 92, 24, WOF_ADD); 
			TBLF AA 1 Bright A_OverlayOffset(Left, 1, 0.25, WOF_ADD);
			TBLF BB 1 Bright A_OverlayOffset(Left, -16, -16, WOF_ADD); 
			TBLF C 1 Bright
			{
					A_FireProjectile("BoostBaronBall", 0, false, -6, 0);
			}
			TBLF C 1 A_OverlayOffset(Left, -64, 0, WOF_ADD); 
			TBLF C 1 A_OverlayOffset(Left, -64, 32, WOF_ADD); 
			TBLF C 1 A_OverlayOffset(Left, -64, 40, WOF_ADD); 
			TNT1 A 1 A_OverlayOffset(Left, 88, 88, WOF_INTERPOLATE);
			TBLG DCBA 1 Bright A_OverlayOffset(Left, -22, -22, WOF_ADD);
			Goto LeftHand;
			
		BoostedNormalFireRight:
			TBRF A 0 A_TakeMana(2, false, "RightHand", 10);
			TBRF A 0
			{
				A_PlaySound("weapons/SpellCast", chan_6);
				A_AlertMonsters();
			}
			TBRG A 1 Bright A_OverlayOffset(Right, -92, 24, WOF_ADD); 
			TBRF AA 1 Bright A_OverlayOffset(Right, -1, 0.25, WOF_ADD);
			TBRF BB 1 Bright A_OverlayOffset(Right, 16, -16, WOF_ADD); 
			TBRF C 1 Bright
			{
					A_FireProjectile("BoostBaronBall", 0, false, 6, 0);
			}
			TBRF C 1 A_OverlayOffset(Right, 64, 0, WOF_ADD); 
			TBRF C 1 A_OverlayOffset(Right, 64, 32, WOF_ADD); 
			TBRF C 1 A_OverlayOffset(Right, 64, 40, WOF_ADD); 
			TNT1 A 1 A_OverlayOffset(Right, -88, 88, WOF_INTERPOLATE);
			TBRG DCBA 1 Bright A_OverlayOffset(Right, 22, -22, WOF_ADD);
			Goto RightHand;
			
		// --------------- FIREBALL ---------------
		
		FireballFireLeft:
			FBLG A 0 A_TakeMana(10, false, "LeftHand", 5);
			FBLG A 0
			{
				A_PlaySound("weapons/FireballCast", 6);
				A_AlertMonsters();
			}
			FBLC A 1 Bright A_OverlayOffset(Left, -24, -3, WOF_ADD);
			FBLC A 1 Bright A_OverlayOffset(Left, -12, -1.5, WOF_ADD);
			FBLF DEF 1 Bright A_OverlayOffset(Left, -1, -0.25, WOF_ADD);
			//FBLC AAAAAA 1 Bright A_OverlayOffset(Left, -6, -2, WOF_ADD);
		FireballFireHoldLeft:
			FBLC BCDEF 1 Bright
			{
				if (CheckInventory("PowerWeaponLevel2", 0) || !(player.cmd.buttons & BT_ATTACK))
				{
					return ResolveState("FireballFireFinishLeft");
				}
				A_ChargeAttack(Left);
				A_OverlayOffset(Left, -55 + frandom(-0.5 * A_GetChargeLevel(Left), 0.5 * A_GetChargeLevel(Left)), -5.5 + frandom(-0.5 * A_GetChargeLevel(Left), 0.5 * A_GetChargeLevel(Left)));
				return ResolveState(null);
			}
			FBLC A 0 Bright A_ReFireHands(Left, "FireballFireHoldLeft");
		FireballFireFinishLeft:
			//FBLC BCD 1 Bright;
			FBLF EFA 1 Bright A_OverlayOffset(Left, 12, -0.15, WOF_ADD);
			FBLF A 1 Bright A_OverlayOffset(Left, 36, -1, WOF_ADD);
			FBLF B 1 Bright A_OverlayOffset(Left, 64, 2, WOF_ADD);
			FBLF C 0 Bright
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
				{
					A_FireProjectile("BoostedFireball", 0, false, -6);
				}
				else
				{
					A_FireProjectile("ExBaronBallCharge"..A_GetChargeLevel(Left), 0, false, -6);
					invoker.ChargeLeft = 0;
				}
			}
			FBLF C 1 Bright A_OverlayOffset(Left, 64, 12, WOF_ADD);
			FBLG A 5 Bright A_OverlayOffset(Left, 0, 105);
			FBLG AABBCCD 1 Bright A_OverlayOffset(Left, 0, -15, WOF_ADD);
			Goto LeftHand;
		
		FireballFireRight:
			FBRG A 0 A_TakeMana(10, false, "RightHand", 5);
			FBRG A 0
			{
				A_PlaySound("weapons/FireballCast", 7);
				A_AlertMonsters();
			}
			FBRC A 1 Bright A_OverlayOffset(Right, 24, -3, WOF_ADD);
			FBRC A 1 Bright A_OverlayOffset(Right, 12, -1.5, WOF_ADD);
			FBRF DEF 1 Bright A_OverlayOffset(Right, 1, -0.25, WOF_ADD);
		FireballFireHoldRight:
			FBRC A 0 A_OverlayOffset(Right, 0, 0);
			FBRC BCDEF 1 Bright
			{
				if (CheckInventory("PowerWeaponLevel2", 0) || !(player.cmd.buttons & BT_ALTATTACK))
				{
					return ResolveState("FireballFireFinishRight");
				}
				A_ChargeAttack(Right);
				A_OverlayOffset(Right, 55 + frandom(-0.5 * A_GetChargeLevel(Right), 0.5 * A_GetChargeLevel(Right)), -5.5 + frandom(-0.5 * A_GetChargeLevel(Right), 0.5 * A_GetChargeLevel(Right)));
				return ResolveState(null);
			}
			FBRC A 0 Bright A_ReFireHands(Right, "FireballFireHoldRight");
		FireballFireFinishRight:
			FBRF EFA 1 Bright A_OverlayOffset(Right, -12, -0.15, WOF_ADD);
			FBRF A 1 Bright A_OverlayOffset(Right, -36, -1, WOF_ADD);
			FBRF B 1 Bright A_OverlayOffset(Right, -64, 2, WOF_ADD);
			FBRF C 0 Bright
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
				{
					A_FireProjectile("BoostedFireball", 0, false, -6);
				}
				else
				{
					A_FireProjectile("ExBaronBallCharge"..A_GetChargeLevel(Right), 0, false, 6);
					invoker.ChargeRight = 0;
				}
			}
			FBRF C 1 Bright A_OverlayOffset(Right, -64, 16, WOF_ADD);
			FBRG A 5 Bright A_OverlayOffset(Right, 0, 105);
			FBRG AABBCCD 1 Bright A_OverlayOffset(Right, 0, -15, WOF_ADD);
			Goto RightHand;
			
		// --------------- BOOSTED FIREBALL ---------------	
			
		BoostedFireballFireLeft:
			TFLF A 0 A_TakeMana(30, false, "LeftHand", 5);
			TFLF A 0
			{
				A_PlaySound("weapons/FireballCast", 6);
				A_AlertMonsters();
			}		
			TFLC A 1 Bright A_OverlayOffset(Left, -36, -3, WOF_ADD);
			TFLC A 1 Bright A_OverlayOffset(Left, -18, -1.5, WOF_ADD);
			TFLC BCD 1 Bright A_OverlayOffset(Left, -1, -0.25, WOF_ADD);
			TFLC DEF 1 Bright;
			TFLC BCDE 1 Bright A_OverlayOffset(Left, 6, -0.15, WOF_ADD);
			TFLF A 1 Bright A_OverlayOffset(Left, 12, -2, WOF_ADD);
			TFLF AA 1 Bright A_OverlayOffset(Left, 24, -2, WOF_ADD);
			TFLF BB 1 Bright A_OverlayOffset(Left, 28, 2, WOF_ADD);
			TFLF C 0 Bright A_FireProjectile("BoostedFireball", 0, false, -6);
			TFLF CCC 1 Bright A_OverlayOffset(Left, 28, 16, WOF_ADD);
			TFLG A 5 Bright A_OverlayOffset(Left, 0, 105);
			TFLG AABBCCD 1 Bright A_OverlayOffset(Left, 0, -15, WOF_ADD);
			Goto LeftHand;
		
		BoostedFireballFireRight:
			TFRF A 0 A_TakeMana(30, false, "RightHand", 5);
			TFRF A 0
			{
				A_PlaySound("weapons/FireballCast", 7);
				A_AlertMonsters();
			}
			TFRC A 1 Bright A_OverlayOffset(Right, 36, -3, WOF_ADD);
			TFRC A 1 Bright A_OverlayOffset(Right, 18, -1.5, WOF_ADD);
			TFRC BCD 1 Bright A_OverlayOffset(Right, 1, -0.25, WOF_ADD);
			TFRC DEF 1 Bright;
			TFRC BCDE 1 Bright A_OverlayOffset(Right, -6, -0.15, WOF_ADD);
			TFRF A 1 Bright A_OverlayOffset(Right, -12, -2, WOF_ADD);
			TFRF AA 1 Bright A_OverlayOffset(Right, -24, -2, WOF_ADD);
			TFRF BB 1 Bright A_OverlayOffset(Right, -28, 2, WOF_ADD);
			TFRF C 0 Bright A_FireProjectile("BoostedFireball", 0, false, 6);
			TFRF CCC 1 Bright A_OverlayOffset(Right, -28, 16, WOF_ADD);
			TFRG A 5 Bright A_OverlayOffset(Right, 0, 105);
			TFRG AABBCCD 1 Bright A_OverlayOffset(Right, 0, -15, WOF_ADD);
			Goto RightHand;
		
		// --------------- IMMOLATION ---------------
		
		ImmolationFireLeft:
			IMLF A 0 A_TakeMana(A_GetChargeLevel(Left) + 1, true, "LeftHand");
			//IMLF A 0 A_PlaySound("weapons/FireballCast2",CHAN_WEAPON);
			IMLF AB 2 Bright; 
			TNT1 A 0 A_SetBlend("GREEN", 0.25, 35);
			IMLF A 0 A_PlaySound ("FWOSH1", CHAN_WEAPON);
		ImmolationHoldLeft:
			IMLF A 0 A_TakeMana(1, false, "ImmolationHoldFinishLeft");
			IMLF C 1 Bright A_OverlayOffset(Left, 0, 0);
			IMLF C 1 Bright
			{
				A_ChargeAttack(Left);
				for (int i = 0; i <= A_GetChargeLevel(Left); ++i)
				{
					A_FireProjectile("BaronFlamethrower", frandom(-7.5, 7.5), false, -8, 8, 0, frandom(-7.5, 7.5));
					A_OverlayOffset(Left, frandom(-1, 1),frandom(-1, 1), WOF_ADD);
				}
			}
			IMLF A 0 A_ReFireHands(Left, "ImmolationHoldLeft");
		ImmolationHoldFinishLeft:
			IMLF BA 2 Bright
			{
				invoker.ChargeLeft = 0;
			}
			Goto LeftHand;
			
		ImmolationFireRight:
			IMRF A 0 A_TakeMana(1, true, "RightHand");
			//IMRF A 0 A_PlaySound("weapons/FireballCast2",CHAN_WEAPON);
			IMRF AB 2 Bright;
			TNT1 A 0 A_SetBlend("GREEN", 0.25, 35);
			IMLF A 0 A_PlaySound ("FWOSH1", CHAN_WEAPON);
		ImmolationHoldRight:
			IMRF A 0 A_TakeMana(A_GetChargeLevel(Right) + 1, false, "ImmolationHoldFinishRight");
			IMRF C 1 Bright A_OverlayOffset(Right, 0, 0);
			IMRF C 1 Bright
			{
				A_ChargeAttack(Right);
				for (int i = 0; i <= A_GetChargeLevel(Left); ++i)
				{
					A_FireProjectile("BaronFlamethrower", frandom(-7.5, 7.5), false, 8, 8, 0, frandom(-7.5, 7.5));
					A_OverlayOffset(Right, frandom(-1, 1),frandom(-1, 1),WOF_ADD);
				}
			}
			IMRF A 0 A_ReFireHands(Right, "ImmolationHoldRight");
		ImmolationHoldFinishRight:
			IMRF BA 2 Bright
			{
				invoker.ChargeRight = 0;
			}
			Goto RightHand;
			
		// --------------- BOOSTED IMMOLATION ---------------		
			
		BoostedImmolationFireLeft:
			BILF A 0 A_TakeMana(40, false, "LeftHand",5);
			BILF A 0 A_PlaySound("weapons/FireballCast2",CHAN_WEAPON);
			BILF ABCDEF 2 Bright;
			BILF F 12 Bright
			{
				A_PlaySound ("Fwosh1", CHAN_Weapon);
				A_FireProjectile("BoostedBaronWave", 0, false, -9);
				A_FireProjectile("BoostedBaronWave", 7.5, false, -9);
				A_FireProjectile("BoostedBaronWave", -7.5, false, -9);
			}
			BILF EDCBA 1 Bright;
			Goto LeftHand;
			
		BoostedImmolationFireRight:
			BIRF A 0 A_TakeMana(40, false, "RightHand",5);
			BIRF A 0 A_PlaySound("weapons/FireballCast2",CHAN_WEAPON);
			BIRF ABCDEF 2 Bright;
			BIRF F 12 Bright
			{
				A_PlaySound ("Fwosh1", CHAN_WEAPON);
				A_FireProjectile("BoostedBaronWave", 0, false, 9);
				A_FireProjectile("BoostedBaronWave", 7.5, false, 9);
				A_FireProjectile("BoostedBaronWave", -7.5, false, 9);
			}
			BIRF EDCBA 1 Bright;
			Goto RightHand;
			
		// --------------- METEOR ---------------
		
		MeteorFireLeft:
			MTLF A 0 A_TakeMana(50, false, "LeftHand", 2);
			MTLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			MTLF AB 2;
			MTLF C 16 A_FireBullets(0,0,1,0,"MeteorTargeter",FBF_NORANDOMPUFFZ);
			MTLF BA 2;
			Goto LeftHand;
			
		MeteorFireRight:
			BBRF A 0 A_TakeMana(50, false, "RightHand", 2);
			BBRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			MTRF AB 2;
			MTRF C 16 A_FireBullets(0,0,1,0,"MeteorTargeter",FBF_NORANDOMPUFFZ);
			MTRF BA 2;
			Goto RightHand;
			
		// --------------- BOOSTED METEOR ---------------
		
		BoostedMeteorFireLeft:
			BMLF A 0 A_TakeMana(50, false, "LeftHand", 2);
			BMLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			BMLF AB 2 Bright;
			BMLF C 16 A_FireBullets(0,0,1,0,"BoostedMeteorTargeter",FBF_NORANDOMPUFFZ);
			BMLF BA 2 Bright;
			Goto LeftHand;
			
		BoostedMeteorFireRight:
			BMRF A 0 A_TakeMana(50, false, "RightHand", 2);
			BMRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			BMRF AB 2 Bright;
			BMRF C 16 A_FireBullets(0,0,1,0,"BoostedMeteorTargeter",FBF_NORANDOMPUFFZ);
			BMRF BA 2 Bright;
			Goto RightHand;
						
		// --------------- DEFILE ---------------
		
		DefileFireLeft:
			CPLF A 0 A_TakeMana(5, false, "LeftHand", 0);
			CPLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			CPLF A 2 Bright; 
			CPLF B 8
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedDefileExploder");
				else A_FireBullets(0, 0, 1, 0, "DefileExploder");
			}
			CPLF A 2 Bright;
			Goto LeftHand;
			
		DefileFireRight:
			CPRF A 0 A_TakeMana(5, false, "RightHand", 0);
			CPRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			CPRF A 2 Bright; 
			CPRF B 8
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedDefileExploder");
				else A_FireBullets(0, 0, 1, 0, "DefileExploder");
			}
			CPRF A 2 Bright;
			Goto RightHand;
			
		// --------------- BOOSTED DEFILE ---------------	
			
		BoostedDefileFireLeft:
			BCLF A 0 A_TakeMana(10, false, "LeftHand", 0);
			BCLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			BCLF A 2 Bright; 
			BCLF B 8 A_FireBullets(0, 0, 1, 0, "BoostedDefileExploder");
			BCLF A 2 Bright;
			Goto LeftHand;
			
		BoostedDefileFireRight:
			BCRF A 0 A_TakeMana(10, false, "RightHand", 0);
			BCRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			BCRF A 2 Bright; 
			BCRF B 8 A_FireBullets(0, 0, 1, 0, "BoostedDefileExploder");
			BCRF A 2 Bright;
			Goto RightHand;
			
		// --------------- HYDRA ---------------
		
		HydraFireLeft:
			HYLF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.HydraCount >= 2 + plr.BaronLevel)
					return ResolveState("LeftHand");
				
				return ResolveState(null);
			}
			HYLF A 0 A_TakeMana(25, false, "LeftHand", 3);
			HYLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			HYLF A 2 Bright;
			HYLF B 12
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedHydraPuff");
				else A_FireBullets(0, 0, 1, 0, "NormalHydraPuff");
			}
			HYLF A 2 Bright;
			Goto LeftHand;
			
		HydraFireRight:
			HYRF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.HydraCount >= 2 + plr.BaronLevel)
					return ResolveState("RightHand");
				
				return ResolveState(null);
			}
			HYRF A 0 A_TakeMana(25, false, "RightHand", 3);
			HYRF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			HYRF A 2 Bright;
			HYRF B 12
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedHydraPuff");
				else A_FireBullets(0, 0, 1, 0, "NormalHydraPuff");
			}
			HYRF A 2 Bright;
			Goto RightHand;
			
		// --------------- BOOSTED HYDRA ---------------
		
		BoostedHydraFireLeft:
			BHLF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.HydraCount >= 2 + plr.BaronLevel)
					return ResolveState("LeftHand");
				
				return ResolveState(null);
			}
			BHLF A 0 A_TakeMana(50, false, "LeftHand", 3);
			BHLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			BHLF A 2 Bright;
			BHLF B 12 A_FireBullets(0, 0, 1, 0, "BoostedHydraPuff");
			BHLF A 2 Bright;
			Goto LeftHand;
			
		BoostedHydraFireRight:
			BHRF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.HydraCount >= 2 + plr.BaronLevel)
					return ResolveState("RightHand");
				
				return ResolveState(null);
			}
			BHRF A 0 A_TakeMana(50, false, "RightHand", 3);
			BHRF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			BHRF A 2 Bright;
			BHRF B 12	A_FireBullets(0, 0, 1, 0, "BoostedHydraPuff");
			BHRF A 2 Bright;
			Goto RightHand;
			
		// --------------- CLONE ---------------
		
		CloneFireLeft:
			CLLF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.CloneCount >= 1 + plr.BaronLevel / 5)
					return ResolveState("LeftHand");
				
				return ResolveState(null);
			}
			CLLF A 0 A_TakeMana(75, false, "LeftHand", 5);
			CLLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			CLLF A 3 Bright;
			CLLF B 16
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedClonePuff");
				else A_FireBullets(0, 0, 1, 0, "NormalClonePuff");
			}
			CLLF A 2 Bright;
			Goto LeftHand;
			
		CloneFireRight:
			CLRF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.CloneCount >= 1 + plr.BaronLevel / 5)
					return ResolveState("RightHand");
				
				return ResolveState(null);
			}
			CLRF A 0 A_TakeMana(75, false, "RightHand", 5);
			CLRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			CLRF A 3 Bright;
			CLRF B 16
			{
				if (CheckInventory("PowerWeaponLevel2", 0))
					A_FireBullets(0, 0, 1, 0, "BoostedClonePuff");
				else A_FireBullets(0, 0, 1, 0, "NormalClonePuff");
			}
			CLRF A 2 Bright;
			Goto RightHand;
			
		// --------------- BOOSTED CLONE ---------------	
			
		BoostedCloneFireLeft:
			TCLF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.CloneCount >= 1 + plr.BaronLevel / 5)
					return ResolveState("LeftHand");
				
				return ResolveState(null);
			}
			TCLF A 0 A_TakeMana(150, false, "LeftHand", 5);
			TCLF A 0
			{
				A_PlaySound("weapons/SpellCast", 6);
				A_AlertMonsters();
			}
			TCLF A 3 Bright;
			TCLF B 16 A_FireBullets(0, 0, 1, 0, "BoostedClonePuff");
			TCLF A 2 Bright;
			Goto LeftHand;
			
		BoostedCloneFireRight:
			TCRF A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.CloneCount >= 1 + plr.BaronLevel / 5)
					return ResolveState("RightHand");
				
				return ResolveState(null);
			}
			TCRF A 0 A_TakeMana(150, false, "RightHand", 5);
			TCRF A 0
			{
				A_PlaySound("weapons/SpellCast", 7);
				A_AlertMonsters();
			}
			TCRF A 3 Bright; 
			TCRF B 16 A_FireBullets(0, 0, 1, 0, "BoostedClonePuff");
			TCRF A 2 Bright;
			Goto RightHand;
	}
}